---
title: Plan testów
client:
project:
---

## Metryczka


|               |             |
| ---           | ---         |
| Identyfikator |             |
| Autorzy       |             |
| Wersja        |             |
| Status        |             |
| Data          |             |
| Poufność      |             |

## Historia zmian
Historia może być utrzymywana jako kolejne wersje dokumentu np. w folderze współdzielonym, repozytorium git, albo jako kolejne wersje dokumentów on-line wiki/confluence/gdocs. Byle było wygodne i spełniało postawione zadania.


| Zmiana | Wersja | Data | Edytor |
| ---    | ---    | ---  | ---    |
|        |        |      |        |
|        |        |      |        |
|        |        |      |        |

# Wstęp
<Cel dokumentu – zbiór wspólnych ustaleń w zakresie strategii testowania, planu testów, harmonogramu, wykonania i dokumentowania testów.>

# Strategia
<Podejście do procesu testowania dla danego projektu i klienta, w szczególności: poziom testowania, sposób testowania, źródło scenariuszy testowych, zakres testów.>

# Poziomy testowania
<Poziomy testowania – testy jednostkowe, modułowe, integracyjne, rodzaje testów akceptacyjnych (IAT, FAT, SAT), praca nadzorowana, testy wydajnościowe.>

# Kryteria wstępne
<Warunki wstępne niezbędne do rozpoczęcia testów – dostępność sprzętu, środowisk testowych, danych, sieci, odpowiednich pomieszczeń, operatorów oraz innych zasobów.>

# Kryteria zakończenia
<Warunki akceptacji testów – kiedy test jest zaakceptowany, kiedy faza testów jest zaakceptowana, określenie działania w sytuacji niedopełnienia zobowiązań którejś ze stron.>

# Organizacja testowania
<Skład zespołów testowych, kompetencje, test manager, raportowanie, itp.>

# Przedmioty testowania
<Przedmioty testowania – okna, raporty, funkcje, interfejsy, algorytmy, konfiguracja, sprzęt, środowisko, sieć itp.>

# Specyfikacja testów
<Określenie zasad tworzenia specyfikacji testów, poszczególnych scenariuszy wraz z określeniem ich rodzajów – np. inne dla interfejsu międzysystemowego, inne dla funkcji.>

# Odpowiedzialność
<Określenie odpowiedzialności stron – klienta, zamawiającego, dostawcy, poddostawcy itp., w kontekście specyfikacji testów, wykonania testów, obsługą dokumentacji itp.>

# Środowisko testowe
<Konfiguracja środowisk testowych – sprzęt, oprogramowanie, interfejsy itp.>

# Harmonogram
<Zaplanowanie faz testowania oraz kamieni milowych.>

# Produkty testowania
<Sposób dokumentowania wykonania testów, rejestracja problemów.>
