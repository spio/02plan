---
title: Plan testów
client: Dla kogo pracujemy?
project: Jaki projekt, podprojekt, etap?
---

## Metryczka


|               |             |
| ---           | ---         |
| Identyfikator | Unikalny, może być identyfikator z issue trackera, JIRA, cokolwiek, byle ułatwić życie. |
| Autorzy       |             |
| Wersja        | Przyjąć jeden schemat numeracji, np. X.Y.ZZ, zakładając także możliwość jasnego określania wydań (np. każda wersja X.Y.00 jest gotowa, po przeglądzie). |
| Status        | Cykl życia dokumentu (w trakcie, gotowy, zaakceptowany itp.) |
| Data          | Data ostatniej aktualizacji |
| Poufność      | Tylko obieg wewnętrzny, używany wspólnie z klientem, publiczny itp. |

## Historia zmian
Historia może być utrzymywana jako kolejne wersje dokumentu np. w folderze współdzielonym, repozytorium git, albo jako kolejne wersje dokumentów on-line wiki/confluence/gdocs. Byle było wygodne i spełniało postawione zadania.


| Zmiana | Wersja | Data | Edytor |
| ---    | ---    | ---  | ---    |
| Co zostało zmienione | Aktualna wersja | Aktualna data | Kto zmienił |
| Przegląd? |        |      | Kto przejrzał |
| Akceptacja? |        |      | Kto zaakceptował |

# Wstęp
<Cel dokumentu – zbiór wspólnych ustaleń w zakresie strategii testowania, planu testów, harmonogramu, wykonania i dokumentowania testów.>

Celem dokumentu jest usystematyzowanie procesu planowania, realizacji i kontroli testowania, przynajmniej w następujących aspektach: strategii, przedmiotów, harmonogramu i planu, specyfikacji, środowisk testowych itp. Dokument reguluje powyższe w ramach organizacji (lub klienta, projektu itp.).

# Strategia
<Podejście do procesu testowania dla danego projektu i klienta, w szczególności: poziom testowania, sposób testowania, źródło scenariuszy testowych, zakres testów.>

Poziomy testowania stanowią przedmiot następnego rozdziału. Testy będą odbywały się w sposób automatyczny (modułów i integracyjne modułów) oraz manualny – zarówno przez programistów jak i testerów z ramienia dostawcy jak i zamawiającego.Podstawą do tworzenia scenariuszy testowych będzie specyfikacja wymagań, przy czym wszystkie scenariusze testowe będą tworzone wspólnie. Szczególną uwagę należy poświęcić testowaniu interfejsów międzysystemowych w kontekście automatyki, z uwagi na wykonanie tej części przez zewnętrzną niezależną firmę.

# Poziomy testowania
<Poziomy testowania – testy jednostkowe, modułowe, integracyjne, rodzaje testów akceptacyjnych (IAT, FAT, SAT), praca nadzorowana, testy wydajnościowe.>

W ramach projektu rozbudowy sortera przewiduje się następujące poziomy testowania:
 - Testy wewnętrzne dostawcy, strukturalne jak i funkcjonalne, modułowe i integracyjne,
 - Akceptacyjne testy wewnętrzne u dostawcy (IAT),
 - Akceptacyjne testy u dostawcy z udziałem klienta (FAT),
 - Akceptacyjne testy u klienta (SAT),
 - Testy przedprodukcyjne z udziałem automatyki, z zakresem jak SAT,
 - Testy interfejsu WMS – MFC,
 - Testy wydajnościowe automatyki wraz z dostarczonym systemem (poza zakresem projektu i dokumentu). Wydajność całego rozwiązania zależy od pracowników obsługujących sorter wysyłkowy, dlatego aspekty wydajnościowe nie należą do kryteriów akceptacji projektu.


# Kryteria wstępne
<Warunki wstępne niezbędne do rozpoczęcia testów – dostępność sprzętu, środowisk testowych, danych, sieci, odpowiednich pomieszczeń, operatorów oraz innych zasobów.>

Przykład:
Testy wewnętrzne (strukturalne, funkcjonalne) będą wykonywane wraz z rozwojem funkcjonalności, tj. warunkiem wstępnym jest dostępność wybranego kawałka systemu.

Warunkiem rozpoczęcia testów IAT jest zakończenie ustalonego zakresu funkcjonalnego. Warunkiem rozpoczęcia testów FAT jest zakończenie IAT z sukcesem i analogicznie, SAT następuje po FAT.

Warunkiem wstępnym rozpoczęcia testów SAT jest udostępnienie sali przez zamawiającego, odpowiednią do zaproszonej grupy uczestników. Testy odbywają się na maszynach dostawcy.

Testy z udziałem automatyki wymagają zakończenia SAT oraz budowy całej wymaganej infrastruktury automatycznej, wraz z jej odbiorem.

...

# Kryteria zakończenia
<Warunki akceptacji testów – kiedy test jest zaakceptowany, kiedy faza testów jest zaakceptowana, określenie działania w sytuacji niedopełnienia zobowiązań którejś ze stron.>

Fazy testów FAT oraz SAT i SAT z automatyką wymagają potwierdzenia przez klienta. Testy uważa się za udane, kiedy nie zgłoszono błędów krytycznych i blokujących a dla błędów pozostałych zaplanowano datę dostarczenia rozwiązania.

# Organizacja testowania
<Skład zespołów testowych, kompetencje, test manager, raportowanie, itp.>

Nadzorem nad procesem testowania zajmuje się aktualny project manager, zarówno po stronie zespołu dostawcy jak i odbiorcy systemu. Każda faza testowania z akceptacją wymaga sporządzenia raportu. Raport określa czy faza została zaakceptowana. Raport sporządzony przez project managera lub osobę wyznaczoną jest przesyłany do managera nadrzędnego.

# Przedmioty testowania
<Przedmioty testowania – okna, raporty, funkcje, interfejsy, algorytmy, konfiguracja, sprzęt, środowisko, sieć itp.>

W ramach projektu przewiduje się testowanie aspektów funkcjonalnych, algorytmy, okna, interfejsy międzysystemowe oraz działanie systemu względem użytkowanej automatyki.

# Specyfikacja testów
<Określenie zasad tworzenia specyfikacji testów, poszczególnych scenariuszy wraz z określeniem ich rodzajów – np. inne dla interfejsu międzysystemowego, inne dla funkcji.>

Każdy przypadek testowy powinien uwzględniać:
 - Identyfikator,
 - Nazwę,
 - Warunki wstępne, stan,
 - Listę kroków,
 - Warunki końcowe, oczekiwany wynik.
 
Przypadki testowe są utrzymywane w systemie JIRA, dokumenty będą generowane na życzenie.

# Odpowiedzialność
<Określenie odpowiedzialności stron – klienta, zamawiającego, dostawcy, poddostawcy itp., w kontekście specyfikacji testów, wykonania testów, obsługą dokumentacji itp.>

W ramach procesu testowania dostawca zobowiązany jest do dostarczenia maszyn na testy IAT, FAT oraz SAT, a dla SAT z automatyką jest to odpowiedzialnością odbiorcy systemu. Zamawiający zobowiązany jest do uprzedniego odbioru automatyki, przed przystąpieniem do testów wspólnych. System JIRA jest udostępniany przez dostawcę.

# Środowisko testowe
<Konfiguracja środowisk testowych – sprzęt, oprogramowanie, interfejsy itp.>

Wszystkie testy wewnętrzne, IAT, FAT będą przeprowadzane na komputerach osobistych z sytemem Ubuntu. Testy SAT oraz SAT z automatyką będę wykonywane na maszynie testowej o konfiguracji będącej odzwierciedleniem maszyny produkcyjnej (tu podać jej parametry). Dla testów bez rzeczywistej automatyki będą stosowane symulatory.

# Harmonogram
<Zaplanowanie faz testowania oraz kamieni milowych.>


# Produkty testowania
<Sposób dokumentowania wykonania testów, rejestracja problemów.>

Wykonanie testów wraz z wynikiem oraz zgłoszone błędy i uwagi będą także rejestrowane w systemie JIRA.
