# Temat
Scenariusze testowe oraz planowanie testowania.

# Cel
Celem zajęć jest wyjaśnienie podstawowych pojęć związanych z procesem testowania oprogramowania, planowania oraz zarządzania testami. W ramach zajęć zostaną przedstawione elementy strategii, faz oraz rodzajów testowania. W trakcie ćwiczenia zostanie wykonany prosty plan testów.

# Środowisko
Ćwiczenia będą wykonywane z wykorzystaniem podstawowych programów biurowych

# Zadanie 1

Należy przygotować dokument planu testów dla zadanego opisu projektu oraz zakresu funkcjonalnego. Należy skorzystać z dostarczonego szablonu planu. Praca w grupach, gotowe dokumenty zostaną omówione na zakończenie zajęć.

## Wprowadzenie
Dokument planu testów pozwala zorganizować proces testowania, przygotowania środowisk testowych oraz pomaga określić odpowiedzialności i sposób realizacji różnych etapów testowania. Plan testów powinien zostać dostosowany do specyfiki biznesu oraz uczestników projektu. Przykładowy plan testowania jest dobrym punktem startowym do planowania procesu testowania, ale nie stanowi jedynej słusznej drogi działania. Konkluzje z przedstawionego planu testowania (np. pytania postaci: czy projekt wymaga integracji z innymi systemami?) dobrze się skalują na różne metodyki wykonawcze (waterfall, agile, dowolne iteracyjne).

## Opis projektu
Operator centrum dystrybucyjnego potrzebuje zwiększyć wydajność systemu w obszarze wysyłki towaru. W tym celu zamierza zbudować automatyczny sorter wysyłkowy, który za pomocą systemu przenośników będzie transportował spakowany towar z obszaru zapakowania na bramy wyjściowe i dalej na samochody. Udział operatorów w procesie powinien być minimalny. Dostawca oprogramowania obsługuje dotychczasową część magazynu (system klasy wms), jest to zatem projekt rozbudowy rozwiązania.

Cel biznesowy klienta: zwiększenie wydajności procesu wysyłki towaru, poprzez zastąpienie procesów manualnych za pomocą automatyki magazynowej.

## Specyfikacja
W ramach projektu przewiduje się zbudowanie systemu przenośników dla 8 bram wysyłkowych, z jednym punktem zasilania oraz jednym punktem wyjaśniania. Dostawcą automatyki oraz oprogramowania niskiego poziomu jest zewnętrzna firma. W punkcie zasilania przewiduje się instalację automatu etykietującego.

Systemy:
1. WMS (warehouse management system) – istniejące oprogramowania do obsługi magazynu,
które jest przedmiotem zmian,
2. MFC (material flow control) – moduł oprogramowania dostawcy automatyki, który zarządza przenośnikami oraz etykieciarką; komunikuje się z systemem wms.

Podstawowe funkcje:
1. Wysłanie zlecenia transportu z systemu wms do mfc, z punktu zasilania na bramę.
2. Wysłanie zlecenia transportu z systemu wms do mfc, z punktu zasilania na miejsce wyjaśniania.
3. Wysłanie potwierdzenia dotarcia do bramy z systemu mfc do wms,
4. Wysłanie potwierdzenia dotarcia do punktu wyjaśniania z systemu mfc do wms,
5. Wysłanie zlecenia wydruku z systemu wms do mfc,
6. Wydruk i naklejenie etykiety sterowane z systemu mfc,
7. Konfiguracja wyboru bram dla spakowanego towaru w systemie wms.

Przewidywane komunikaty:
1. Wysłanie zlecenia transportowego z systemu wms do mfc,
2. Wysłanie zlecenia wydruku z systemu wms do mfc,
3. Wysłanie potwierdzenia dotarcia do miejsca z systemu mfc do wms.

Środowisko systemu wms:
1. System operacyjny – RedHat Linux,
2. System bazy danych – Oracle 11g.

Środowisko systemu mfc:
1. System operacyjny – Windows Server.

Sposób komunikacji pomiędzy systemami wms i mfc:
1. TCP/IP, dwa kanały dwukierunkowe.

## Wycena
Wstępna wycena w roboczogodzinach. Wycena nie zawiera kosztów delegacji.
* Analiza, specyfikacja – 120h
* Implementacja – 400h
* Wdrożenie – 120h
* Dokumentacja – 24h
* Testowanie - ?
* Zarządzanie projektem – 80h
